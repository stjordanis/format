# Format

Disk formatting program -- creates FAT file systems and lowlevel formats floppy disks


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## FORMAT.LSM

<table>
<tr><td>title</td><td>Format</td></tr>
<tr><td>version</td><td>0.91x</td></tr>
<tr><td>entered&nbsp;date</td><td>2017-09-19</td></tr>
<tr><td>description</td><td>Disk formatting program -- creates FAT file systems and lowlevel formats floppy disks</td></tr>
<tr><td>keywords</td><td>freedos, format, fat12, fat16, fat32, floppy, harddisk, mirror, unformat</td></tr>
<tr><td>author</td><td>Brian E. Reifsnyder &lt;reifsnyderb (#) mindspring.com&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/FDOS/format</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU GPL](LICENSE)</td></tr>
</table>
